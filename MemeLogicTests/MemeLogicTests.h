//
//  MemeLogicTests.h
//  MemeLogicTests
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Tests the Meme model.
//

#import <SenTestingKit/SenTestingKit.h>
#import "Meme.h"

@interface MemeLogicTests : SenTestCase

@property (readwrite, nonatomic, strong) Meme *meme;

@end
