//
//  MemeLogicTests.m
//  MemeLogicTests
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Tests the Meme model.
//

#import "MemeLogicTests.h"

@implementation MemeLogicTests

@synthesize meme = _meme;

- (void)setUp
{
    [super setUp];
    
    // give the meme an image from the web so as to not simulaneously test Library model
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: @"http://1.bp.blogspot.com/-iJH4pMYgtO4/TZu2-E4HzoI/AAAAAAAAAK4/tGI2hm6_1A8/s1600/14.%2BRickroll.jpg"]];
    UIImage * image = [UIImage imageWithData: imageData];
    
    // initialize the meme with our convenience method
    self.meme = [[Meme alloc] initWithImage:image topText:@"Test Top" bottomText:@"Test Bottom" andTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
}

// tests initWithImage:topText:bottomText:andTextColor: convenience initialization method
- (void)testConvenienceInit
{
    // make sure method returns the instance class
    STAssertNotNil(self.meme, @"init returned nil");
    
    // make sure that the method has updated all of the class's properties correctly
    STAssertTrue([self.meme.topText isEqualToString:@"Test Top"] == YES, @"setting of top text didn't work");
    STAssertTrue([self.meme.bottomText isEqualToString:@"Test Bottom"] == YES, @"setting of bottom text didn't work");
    STAssertTrue([self.meme.textColor isEqual:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]], @"setting of text color didn't work");
    STAssertNotNil(self.meme.image, @"setting of image didn't work");
}

- (void)tearDown
{
    [super tearDown];
}

@end
