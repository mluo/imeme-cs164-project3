//
//  MainViewController.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view allowing user to input text for the meme, 
//  choose font color, and access the three different ways of choosing an image.
//  This is the view initially presented to the user upon startup of app.
//

#import "FlipsideViewController.h"
#import "LibraryViewController.h"
#import "Library.h"
#import "Meme.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, 
                                                  LibraryViewControllerDelegate,
                                                  UIImagePickerControllerDelegate,
                                                  UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *topText;
@property (weak, nonatomic) IBOutlet UITextField *bottomText;
@property (weak, nonatomic) IBOutlet UISegmentedControl *textColorSelector;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) LibraryViewController *libraryViewController;
@property (strong, nonatomic) FlipsideViewController *flipsideViewController;
@property (strong, nonatomic) Library *library;

// displays FlipSideViewController with meme image
- (IBAction)showMeme:(id)sender;

// displays LibraryViewController, which allows user to pick an image to use in the meme
- (IBAction)pickFromLibrary:(id)sender;

// responds to user pressing "Pick from Camera Roll" button
- (IBAction)pickFromCameraRoll:(id)sender;

// responds to user pressing "Take Photo" button
- (IBAction)takePhoto:(id)sender;

// Called to hide keyboard if needed
- (void)resignTextFields:(id)selector;

@end
