//
//  Meme.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 
//  This model encapsulates data needed to create a meme.
//

#import <Foundation/Foundation.h>

@interface Meme : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *topText;
@property (nonatomic, strong) NSString *bottomText;
@property (nonatomic, strong) UIColor *textColor;

// initializes an image with relevant instance variables
-(id)initWithImage:(UIImage *)image topText:(NSString *)topText bottomText:(NSString *)bottomText andTextColor:(UIColor *)textColor;

@end
