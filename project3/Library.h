//
//  Library.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 
//  This model handles the organization of our library of meme images.
//

#import <Foundation/Foundation.h>

@interface Library : NSObject

@property (nonatomic, readwrite, strong) NSArray *imagePaths;
@property (nonatomic, readwrite, strong) NSArray *imageURLs;
@property (nonatomic, assign) int nativeImageCount;
@property (nonatomic, assign) int onlineImageCount;

// initializes the library by setting imagePaths array and the imageCount int
- (id)initWithImagePaths:(NSArray *)paths;

// adds URLs to library
- (void)addURLs:(NSArray *)urls;

@end
