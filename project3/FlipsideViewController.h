//
//  FlipsideViewController.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view that showcases your completed meme fullscreen.
//  Allows user to "save" the meme to their Camera Roll.
//

#import <UIKit/UIKit.h>
// for screenshot
#import <QuartzCore/QuartzCore.h>
// for camera sound
#import <AudioToolbox/AudioServices.h>
#import "Meme.h"

@class FlipsideViewController;


// the MainViewController will be a FlipsideViewController Delegate
@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end


@interface FlipsideViewController : UIViewController

// MainViewController will the delegate
@property (weak, nonatomic) IBOutlet id <FlipsideViewControllerDelegate> delegate;
// for temporary encapsulation of the meme's data
@property (strong, nonatomic) Meme *meme;
// meme data ultimately gets pushed to the screen with these variables
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UILabel *topText;
@property (weak, nonatomic) IBOutlet UILabel *bottomText;

// returns user to main view
- (IBAction)done:(id)sender;

// allows user to save meme to Camera Roll via screenshot
- (IBAction)save:(id)sender;

// responds to when user taps the screen, calls fadeNavBarInOrOut:
- (IBAction)touchImage:(id)sender;

// fades the navigation bar to fade in and out
- (void)fadeNavBarInOrOut:(NSString *)inOrOut;

@end
