Project 3
CS 164
TF: Chris Gerber

Michelle Luo and Evan Wu 

README for Release Version

In this version of our app, you can
    a) Choose an image from our library of common meme images on a slide-up view.
    b) Add custom text and choose the font color of the text in the main view.
    c) View the completed meme fullscreen on the flipside view. 
    d) Choose an image from the phone's Camera Roll.
    e) Take a photo from within the app to use as the meme image.
    f) "Save" a meme to the Camera Roll by taking a screenshot of the flipside view.

We would like to highlight that the library of common meme images includes an online repo of images (stored on Evan's cloud account). We can add or delete images from the repo by uploading them to or removing them from the cloud, then modifying a comma-separated list of image names (also stored on the cloud) to reflect these changes. Each time the app is backgrounded and then opened again, the images from the online repo are refreshed. These images are meant to be aesthetically indistinguishable from the images that are included with the app, but notice that the last 3 images here are from the online repo -- they will only appear if you have an internet connection, and are not included with the project files.

One thing to keep in mind while testing is that the simulator does not have a camera. Therefore, if you try to test taking a photo from within the app to use as the meme image and you are running the app on a simulator, the app will crash. All the other features should work on the simulator, and all of the features should work if you are using a physical device.

Make sure you're testing with the sound on if you'd like to hear the shutter click when you save your memes!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

README for Alpha Version

In this version of our app, you can 
    a) Choose an image from our library of common meme images on a slide-up view;
    b) Add custom text and choose the font color of the text in the main view; and
    c) View the completed meme fullscreen on the flipside view. 

In the Release Version, we will also implement the following features:
    a) Choosing an image from the phone's Camera Roll;
    b) Taking a photo from within the app to use as the meme image;
    c) "Saving" a meme by taking a screenshot of the flipside view.

Although we discussed removing the Meme model from our code, we ultimately decided to preserve it. The model functions essentially as a struct with a convenient initialization method. We decided to use the Meme model because it is a nice container with which to ship our data from the MainViewController to the FlipsideViewController. It seems that you cannot set the UIOutlet variables of the FlipsideViewController from the MainViewController, because when the flipside view is finally presented, the UIOutlet variables default to the placeholders in the .xib. Therefore, since we have to use temporary variables in the FlipsideViewController to store the meme data before using those temporary variables to set the actual UIOutlet variables in the viewDidLoad method, we decided we might as well encapsulate those temporary variables in the Meme model.

We have not yet deleted a lot of the dead code in our repository because at at this stage of development, we are unsure if we will eventually need that dead code. 

We have the navigation bar of the flipside view fade out by itself after a second. Do you like this?

Please note that the three warnings generated during the compilation are all due to code we borrowed from another source. We are unsure how to fix these warnings. Please advise.