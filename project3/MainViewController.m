//
//  MainViewController.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view allowing user to input text for the meme, 
//  choose font color, and access the three different ways of choosing an image.
//  This is the view initially presented to the user upon startup of app.
//

#import "MainViewController.h"
#import "UISegmentedControlExtension.h"

// for segmented control color selector
#define BLACK 0
#define WHITE 1
#define RED 2
#define GREEN 3
#define BLUE 4

@implementation MainViewController

@synthesize topText = _topText;
@synthesize bottomText = _bottomText;
@synthesize imageView = _imageView;
@synthesize textColorSelector = _textColorSelector;
@synthesize library = _library;
@synthesize libraryViewController = _libraryViewController;
@synthesize flipsideViewController = _flipsideViewController;

#pragma mark - View lifecycle

// Perform additional setup of view after it loads
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set titles for segments of color selector
    [self.textColorSelector setTitle:@"black" forSegmentAtIndex:BLACK];
    [self.textColorSelector setTitle:@"white" forSegmentAtIndex:WHITE];
    [self.textColorSelector setTitle:@"red" forSegmentAtIndex:RED];
    [self.textColorSelector setTitle:@"green" forSegmentAtIndex:GREEN];
    [self.textColorSelector setTitle:@"blue" forSegmentAtIndex:BLUE];
    
    // set title to be visible for selected segment of color selector
    UIColor *color = [UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:color forKey:UITextAttributeTextColor];
    [self.textColorSelector setTitleTextAttributes:attributes forState:UIControlStateSelected];
    
    // set title to be invisible for other segments for color selector
    color = [UIColor colorWithWhite:0 alpha:0];
    attributes = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:color, color, nil] forKeys:[NSArray arrayWithObjects:UITextAttributeTextColor, UITextAttributeTextShadowColor, nil]];
    [self.textColorSelector setTitleTextAttributes:attributes forState:UIControlStateNormal];    
    
    // Make background colors of segmented control segments using UISegmentedControlExtension
    // First set tags to comply with extension
    [self.textColorSelector setTag:BLACK forSegmentAtIndex:BLACK];
    [self.textColorSelector setTag:WHITE forSegmentAtIndex:WHITE];
    [self.textColorSelector setTag:RED forSegmentAtIndex:RED];
    [self.textColorSelector setTag:GREEN forSegmentAtIndex:GREEN];
    [self.textColorSelector setTag:BLUE forSegmentAtIndex:BLUE];
    
    // Now set the background colors
    // Use off-black and off-white colors so that selection is noticeable
    [self.textColorSelector setTintColor:[UIColor darkGrayColor] forTag:BLACK];
    [self.textColorSelector setTintColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0] forTag:WHITE];
    [self.textColorSelector setTintColor:[UIColor redColor] forTag:RED];
    [self.textColorSelector setTintColor:[UIColor greenColor] forTag:GREEN];
    [self.textColorSelector setTintColor:[UIColor blueColor] forTag:BLUE];
    
    // resign keyboard if color selection made
    [self.textColorSelector addTarget:self action:@selector(resignTextFields:) forControlEvents:UIControlEventValueChanged];
    
}

// Called to hide keyboard if needed
- (void)resignTextFields:(id)selector
{
    [self.topText resignFirstResponder];
    [self.bottomText resignFirstResponder];
}

// Called when user presses return, capitalizes input text
- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
    // Hide keyboard if user presses return
    [textField resignFirstResponder];
    
    // Make text all caps
    textField.text = [textField.text uppercaseString];
    
    return YES;
}

// Called when user presses other text field, capitalizes input text
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    // Make text all caps
    textField.text = [textField.text uppercaseString];
    
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // only allow upright orientation
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Flipside View

// Called when flipside view controller finishes
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self resignTextFields:self];
    [self dismissViewControllerAnimated:YES completion:NULL];    
}

// Called when user presses "Make Meme" button, prepares meme and switches to flipside view
- (IBAction)showMeme:(id)sender
{    
    self.flipsideViewController = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
    self.flipsideViewController.delegate = self;
    self.flipsideViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    // determine which text color has been selected
    UIColor *textColor;
    switch ([self.textColorSelector selectedSegmentIndex]) {
        case BLACK:
            textColor = [UIColor blackColor];
            break;
        case WHITE:
            textColor = [UIColor whiteColor];
            break;
        case RED:
            textColor = [UIColor redColor];
            break;
        case GREEN:
            textColor = [UIColor greenColor];
            break;
        case BLUE:
            textColor = [UIColor blueColor];
            break;
        default:
            textColor = [UIColor blackColor];
            break;
    }
    
    // initialize a meme instance to facilitate passing variables between the MainViewController and FlipsideViewController
    Meme *meme = [[Meme alloc] initWithImage:self.imageView.image topText:self.topText.text bottomText:self.bottomText.text andTextColor:textColor];

    self.flipsideViewController.meme = meme;
    
    [self presentViewController:self.flipsideViewController animated:YES completion:NULL];    
}

#pragma mark - Libary View

// Called upon selection of an image from the library
- (void)libraryViewControllerDidFinish:(LibraryViewController *)controller andImageWasSelected:(BOOL)imageWasSelected
{
    if (imageWasSelected == YES) {
        self.imageView.image = controller.selectedImage;
    }
    
    [self resignTextFields:self];
    
    [self dismissViewControllerAnimated:YES completion:NULL];    
}

// Called when "Pick from iMeme Library" button is pressed, brings up images from the library, where the user can click on an image to select it to use for the meme
- (IBAction)pickFromLibrary:(id)sender
{
    self.libraryViewController = [[LibraryViewController alloc] initWithNibName:@"LibraryViewController" bundle:nil];
    self.libraryViewController.delegate = self;
    self.libraryViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    // pass the library to the LibraryViewController
    self.libraryViewController.library = self.library;
    
    [self resignTextFields:self];
    
    [self presentViewController:self.libraryViewController animated:YES completion:NULL];        
}

#pragma mark - Image Picker

// responds to user pressing "Pick from Camera Roll" button
- (IBAction)pickFromCameraRoll:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = (id) self;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker animated:YES completion:NULL];    
}

// responds to user pressing "Take Photo" button
- (IBAction)takePhoto:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = (id) self;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

// responds to user picking a photo either through camera roll or taking a photo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    self.imageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:NULL];    
}

@end
