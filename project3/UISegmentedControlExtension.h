//
//  UISegmentedControlExtension.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  This code is copied from:
//  http://www.framewreck.net/2010/07/custom-tintcolor-for-each-segment-of.html
//
//  This extension allows us to create UISegmentedControls with custom color schemes. 
//

@interface UISegmentedControl(CustomTintExtension)

// sets a tag for a segment
-(void)setTag:(NSInteger)tag forSegmentAtIndex:(NSUInteger)segment;

// sets background color for a segment with specified aTag
-(void)setTintColor:(UIColor *)color forTag:(NSInteger)aTag;

// sets text color for a segment with specified aTag
-(void)setTextColor:(UIColor *)color forTag:(NSInteger)aTag;

// sets shadow color for a segment with specified aTag
-(void)setShadowColor:(UIColor *)color forTag:(NSInteger)aTag;

@end
