//
//  FlipsideViewController.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view that showcases your completed meme fullscreen.
//  Allows user to "save" the meme to their Camera Roll.
//

#import "FlipsideViewController.h"

// for fading the navigation bar in and out
#define VISIBLE_ALPHA 0.8
#define INVISIBLE_ALPHA 0

@implementation FlipsideViewController

@synthesize meme = _meme;
@synthesize delegate = _delegate;
@synthesize topText = _topText;
@synthesize bottomText = _bottomText;
@synthesize navBar = _navBar;
@synthesize imageView = _imageView;

#pragma mark - View lifecycle

// set up the view with the meme before it appears
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // make sure navigation bar is visible
    self.navBar.alpha = VISIBLE_ALPHA;
    
    // set font to Impact
    self.topText.font = [UIFont fontWithName:@"Impact" size:36.0];
    self.bottomText.font = [UIFont fontWithName:@"Impact" size:36.0];
    
    // set the text according to user desires
    self.topText.text = self.meme.topText;
    self.bottomText.text = self.meme.bottomText;
    self.topText.textColor = self.meme.textColor;
    self.bottomText.textColor = self.meme.textColor;
    
    // position top text 
    [self.topText sizeToFit];
    CGRect frame = CGRectMake((self.imageView.bounds.size.width - self.topText.bounds.size.width) / 2, 5, self.topText.bounds.size.width, self.topText.bounds.size.height);
    [self.topText setFrame:frame];    
    
    // position bottom text
    [self.bottomText sizeToFit];
    frame = CGRectMake((self.imageView.bounds.size.width - self.bottomText.bounds.size.width) / 2, self.imageView.bounds.size.height - (self.bottomText.bounds.size.height + 5), self.bottomText.bounds.size.width, self.bottomText.bounds.size.height);
    [self.bottomText setFrame:frame];
    
    // use the image chosen by the user
    self.imageView.image = self.meme.image;
    
}

// do additional stuff to the view after it has appeared
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // automatically fade the navigation bar out so user realizes this functionality
    sleep(0.5);
    [self fadeNavBarInOrOut:@"out"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // only allow upright orientation
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

// return to main view for further editing
- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

// saves meme by taking a screenshot and saving it to the camera roll
- (IBAction)save:(id)sender
{
    // quickly hide navBar
    self.navBar.alpha = INVISIBLE_ALPHA;
    
    // Capture screenshot
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(screenshotImage, nil, nil, nil);
    
    // Play sound
    SystemSoundID cameraSound;
    NSString *soundPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"CLICK" ofType:@"m4a"];
    CFURLRef soundURL = (__bridge CFURLRef)[NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID(soundURL, &cameraSound);
    AudioServicesPlaySystemSound(cameraSound);

    // fade in navBar
    [self fadeNavBarInOrOut:@"in"];    
}

// responds to when user touches image
- (IBAction)touchImage:(id)sender 
{
    // if navBar is already visible, fade out
    if (self.navBar.alpha > INVISIBLE_ALPHA)
        [self fadeNavBarInOrOut:@"out"];
    // if navBar is already hidden, fade in
    else if (self.navBar.alpha == INVISIBLE_ALPHA)
        [self fadeNavBarInOrOut:@"in"];
}

// fades navigation bar in or out
// request parameter should be either @"out" or @"in" depending on whether you want to fade the navigation bar out or in, respectively
- (void)fadeNavBarInOrOut:(NSString *)request
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    if (request == @"in")
        self.navBar.alpha = VISIBLE_ALPHA;
    else if (request == @"out")
        self.navBar.alpha = INVISIBLE_ALPHA;
    [UIView commitAnimations];
}

@end
