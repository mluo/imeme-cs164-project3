//
//  Meme.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 
//  This model encapsulates data needed to create a meme. 
//  Also provides a convenient initialization method.
//

#import "Meme.h"

@implementation Meme

@synthesize image = _image;
@synthesize topText = _topText;
@synthesize bottomText = _bottomText;
@synthesize textColor = _textColor;

// takes in an image, string for topText, string for bottomText, and color for fontColor and initializes and instance of a Meme accordingly
-(id)initWithImage:(UIImage *)image topText:(NSString *)topText bottomText:(NSString *)bottomText andTextColor:(UIColor *)textColor 
{
    if (self = [super init]) {
        self.image = image;
        self.topText = topText;
        self.bottomText = bottomText;
        self.textColor = textColor;
    }
    
    return self;
}

@end

