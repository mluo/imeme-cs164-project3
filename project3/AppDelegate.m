//
//  AppDelegate.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 
//  Performs default AppDelegate functions and also loads our library of meme images.
//

#import "AppDelegate.h"
#import "MainViewController.h"

#define ONLINE_REPO @"http://cloud.cs50.net/~evanwu/iMemeImages/"

@implementation AppDelegate

@synthesize window = _window;
@synthesize mainViewController = _mainViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];
    
    
    // load the paths of the native images
    
    // retrieve array of full file paths to all the images stored in the app
    NSArray *filePaths = [[NSBundle bundleForClass:[self class]]  pathsForResourcesOfType:@"jpg" inDirectory:@"/"];
    // initialize a new array that will hold the shorted file paths
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[filePaths count]];
    // iterate through the array of full file paths and store just the last component of each image name (i.e. "myImage.jpg" instead of "~/project3.app/myImage.jpg")
    int numFilePaths = [filePaths count];
    for (int i = 0; i < numFilePaths; i++) {
        [images insertObject:[[filePaths objectAtIndex:i] lastPathComponent] atIndex:i];
    }
    // initialize a library instance and pass this to the MainViewController
    self.mainViewController.library = [[Library alloc] initWithImagePaths:images];

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    // refresh image library each time application becomes active
    // load the URLs of online images from online repo's index.csv
    
    // get base URL of repo
    NSString *indexURL = [[NSString alloc] initWithString:ONLINE_REPO];
    // append index.txt to URL
    indexURL = [indexURL stringByAppendingString:@"index.txt"];
    // get the index of image names
    NSString *index = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:indexURL] encoding:NSUTF8StringEncoding error:nil];
    // put the image names in an array
    NSArray *imageNames = [[NSArray alloc] initWithArray:[index componentsSeparatedByString:@", "]];
    // prepend the base url to each image name
    int numImages = [imageNames count];
    NSMutableArray *urls = [[NSMutableArray alloc] initWithCapacity:numImages];
    for (int i = 0; i < numImages; i++) {
        NSString *url = [[NSString alloc] initWithString:ONLINE_REPO];
        url = [url stringByAppendingString:[imageNames objectAtIndex:i]];
        [urls addObject:url];
    }
    // add the URLs to the library
    [self.mainViewController.library addURLs:urls];
}

@end
