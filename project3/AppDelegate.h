//
//  AppDelegate.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MainViewController *mainViewController;

@end
