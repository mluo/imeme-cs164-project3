//
//  UISegmentedControlExtension.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  This code is copied from:
//  http://www.framewreck.net/2010/07/custom-tintcolor-for-each-segment-of.html
//
//  This extension allows us to create UISegmentedControls with custom color schemes. 
//

#import "UISegmentedControlExtension.h"

@implementation UISegmentedControl(CustomTintExtension)

// sets a tag for a segment
-(void)setTag:(NSInteger)tag forSegmentAtIndex:(NSUInteger)segment {
    [[[self subviews] objectAtIndex:segment] setTag:tag];
}

// sets background color for a segment with specified aTag
-(void)setTintColor:(UIColor *)color forTag:(NSInteger)aTag {
    // must operate by tags b/c subview index is unreliable
    UIView *segment = [self viewWithTag:aTag];
    
    // if the segment exists and if it responds to the setTintColor message
    if (segment && ([segment respondsToSelector:@selector(setTintColor:)])) {
        [segment performSelector:@selector(setTintColor:) withObject:color];
    }
}

// sets text color for a segment with specified aTag
-(void)setTextColor:(UIColor *)color forTag:(NSInteger)aTag {
    UIView *segment = [self viewWithTag:aTag];
    for (UIView *view in segment.subviews) {
        // if the sub view exists and if it responds to the setTextColor message
        if (view && ([view respondsToSelector:@selector(setTextColor:)])) {
            [view performSelector:@selector(setTextColor:) withObject:color];
        }
    }
}

// sets shadow color for a segment with specified aTag
-(void)setShadowColor:(UIColor *)color forTag:(NSInteger)aTag {
    UIView *segment = [self viewWithTag:aTag];
    for (UIView *view in segment.subviews) {
        if (view && ([view respondsToSelector:@selector(setShadowColor:)])) {
            [view performSelector:@selector(setShadowColor:) withObject:color];
        }
    }
}

@end