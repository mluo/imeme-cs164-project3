//
//  Library.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
// 
//  This model handles the organization of our library of meme images. 
//  Also provides a method used to retrieve the paths to our images.
//

#import "Library.h"

@implementation Library

@synthesize imagePaths = _imagePaths;
@synthesize imageURLs = _imageURLs;
@synthesize nativeImageCount = _nativeImageCount;
@synthesize onlineImageCount = _onlineImageCount;

// takes in an NSArray of strings (file paths) and initializes the array associated with the library, as well as stores the number of native images in the library
- (id)initWithImagePaths:(NSArray *)paths
{
    if (self = [super init]) {
        self.imagePaths = paths; //[[NSMutableArray alloc] initWithCapacity:[paths count]];
        self.nativeImageCount = [paths count];
    }
    
    return self;
}

// takes in an NSArray of strings (URLs) and adds them to the imageURLs property, as well as stores the number of online images in the library
- (void)addURLs:(NSArray *)urls 
{
    self.imageURLs = urls;
    self.onlineImageCount = [urls count];
}

@end
