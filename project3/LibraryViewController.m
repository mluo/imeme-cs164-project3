//
//  LibraryViewController.m
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view with thumbnails of our provided meme images. 
//  Allows user to pick an image.
//

#import "LibraryViewController.h"

@implementation LibraryViewController

@synthesize delegate = _delegate;
@synthesize library = _library;
@synthesize scrollView = _scrollView;
@synthesize selectedImage = _selectedImage;

#pragma mark - View lifecycle

// populate the view with thumbnails of the meme images
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // for testing purposes
    //NSLog(@"View did load");
    
    // enable scrolling
    self.scrollView.scrollEnabled = YES;
    
    // add images to view
    for (int i = 0; i < self.library.nativeImageCount + self.library.onlineImageCount; i++) {
        // for testing purposes
        //NSLog([self.library.imagePaths objectAtIndex:i]);
        
        // determine the width of each image, based on the bounds of the screen
        float width = (CGRectGetWidth(self.scrollView.bounds) - 10) / 4;
        
        // initialize frame to bound the size of the image
        CGRect frame = CGRectMake(5 + width * (i % 4), 5 + 110 * (i / 4), 70, 105);
        
        // initialize a button
        UIButton *image = [[UIButton alloc] initWithFrame:frame];
        
        // set the image on the button to native images
        if (i < self.library.nativeImageCount) 
            [image setImage:[UIImage imageNamed:[self.library.imagePaths objectAtIndex:i]] forState:UIControlStateNormal];
        // set the image on the button to online images
        else {
            NSURL *onlineImageURL = [NSURL URLWithString:[self.library.imageURLs objectAtIndex:(i - self.library.nativeImageCount)]];
            NSData *onlineImageData = [NSData dataWithContentsOfURL:onlineImageURL];
            [image setImage:[UIImage imageWithData:onlineImageData] forState:UIControlStateNormal];
        }
        
        // wire the button to the selectImage method
        [image addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchUpInside];
        
        // add the button to the view
        [self.scrollView addSubview:image];
        
        // this is for application testing purposes
        if (i == 1) {
            [image setTag:1];
        }
    }
    
    // tell scrollView how long to make the page
    // length is top buffer + (image height + bottom buffer for each row) * number of rows
    int numRows = (self.library.nativeImageCount + self.library.onlineImageCount + 3) / 4;
    self.scrollView.contentSize = CGSizeMake(320, 5 + (105 + 5) * numRows);    
}

//  when user presses an image button, stores the image associated with that button and returns to MainViewController
- (IBAction)selectImage:(id)sender
{
    UIButton *imageButton = (UIButton *)sender;
    self.selectedImage = imageButton.imageView.image;
    [self.delegate libraryViewControllerDidFinish:self andImageWasSelected:YES];
}

// response if user presses cancel button (returns to MainViewController without storing tag of image)
- (IBAction)cancel:(id)sender
{
    [self.delegate libraryViewControllerDidFinish:self andImageWasSelected:NO];
}

@end
