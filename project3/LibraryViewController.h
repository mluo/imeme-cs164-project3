//
//  LibraryViewController.h
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Presents a view with thumbnails of our provided meme images. 
//  Allows user to pick an image.

#import <UIKit/UIKit.h>
#import "Library.h"

@class LibraryViewController;


// the MainViewController will be a LibraryViewController Delegate
@protocol LibraryViewControllerDelegate
- (void)libraryViewControllerDidFinish:(LibraryViewController *)controller andImageWasSelected:(BOOL)andImageWasSelected;
@end


@interface LibraryViewController : UIViewController

// MainViewController will act as delegate
@property (weak, nonatomic) IBOutlet id <LibraryViewControllerDelegate> delegate;
@property (weak, nonatomic) Library *library;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage * selectedImage;

// selects tapped image (button) and returns to MainViewController
- (IBAction)selectImage:(id)sender;

// returns to MainViewController without selecting an image
- (IBAction)cancel:(id)sender;

@end
