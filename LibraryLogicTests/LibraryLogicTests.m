//
//  LibraryLogicTests.m
//  LibraryLogicTests
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Tests the Library model.
//

#import "LibraryLogicTests.h"

@implementation LibraryLogicTests

@synthesize library = _library;

- (void)setUp
{
    [super setUp];
    
    // init the library with some test paths
    NSArray *testPaths = [NSArray arrayWithObjects:@"test1.jpg", @"test2.jpg", nil];
    self.library = [[Library alloc] initWithImagePaths:testPaths];
    
    // add to the library some test URLs
    NSArray *testURLs = [NSArray arrayWithObjects:@"http://www.test.com/image1.jpg", @"http://www.test.com/image2.jpg", @"http://www.test.com/image3.jpg", nil];
    [self.library addURLs:testURLs];
}

// tests initWithImagePaths: convenience initialization method
- (void)testConvenienceInit
{
    // make sure method returns the instance class
    STAssertNotNil(self.library, @"init returned nil");
    
    // make sure that the method has updated all of the class's properties correctly
    STAssertTrue([[self.library.imagePaths objectAtIndex:0] isEqualToString:@"test1.jpg"] == YES, @"setting of image path 1 didn't work");
    STAssertTrue([[self.library.imagePaths objectAtIndex:1] isEqualToString:@"test2.jpg"] == YES, @"setting of image path 2 didn't work");
    STAssertTrue(self.library.nativeImageCount == 2, @"counting of image paths didn't work");
}

// tests addURLs: method
-(void)testAddURLs
{
    // make sure that the method has updated all of the class's properties correctly
    STAssertTrue([[self.library.imageURLs objectAtIndex:0] isEqualToString:@"http://www.test.com/image1.jpg"] == YES, @"setting of image URL 1 didn't work");
    STAssertTrue([[self.library.imageURLs objectAtIndex:1] isEqualToString:@"http://www.test.com/image2.jpg"] == YES, @"setting of image URL 2 didn't work");
    STAssertTrue([[self.library.imageURLs objectAtIndex:2] isEqualToString:@"http://www.test.com/image3.jpg"] == YES, @"setting of image URL 3 didn't work");
    STAssertTrue(self.library.onlineImageCount == 3, @"counting of image paths didn't work");
}

- (void)tearDown
{
    [super tearDown];
}

@end
