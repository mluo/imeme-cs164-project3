//
//  LibraryLogicTests.h
//  LibraryLogicTests
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Tests the Library model.
//

#import <SenTestingKit/SenTestingKit.h>
#import "Library.h"

@interface LibraryLogicTests : SenTestCase

@property (nonatomic, strong) Library *library;

@end
