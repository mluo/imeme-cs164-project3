//
//  project3Tests.m
//  project3Tests
//  project3
//  CS 164
//  Michelle Luo and Evan Wu
//
//  Application tests for the views.
//

#import "project3Tests.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "LibraryViewController.h"
#import "FlipsideViewController.h"
#import "Meme.h"

#define BLACK 0
#define WHITE 1
#define RED 2
#define GREEN 3
#define BLUE 4

#define VISIBLE_ALPHA 0.8
#define INVISIBLE_ALPHA 0

@interface project3Tests ()

@property (nonatomic, readwrite, weak) AppDelegate *appDelegate;
@property (nonatomic, readwrite, weak) MainViewController *mainViewController;
@property (nonatomic, readwrite, weak) UIView *mainView;

@end

@implementation project3Tests

@synthesize appDelegate = _appDelegate;
@synthesize mainViewController = _mainViewController;
@synthesize mainView = _mainView;


- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.mainViewController = self.appDelegate.mainViewController;
    self.mainView = self.mainViewController.view;
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];

}

/*- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in project3Tests");
}*/

- (void)testAppDelegate
{
    STAssertNotNil(self.appDelegate, @"Cannot find the application delegate");
}

- (void)testPressingCancelForLibrary
{
    // make sure there's no image in main view to begin with
    self.mainViewController.imageView.image = nil;
    
    // set up library view controller
    [self.mainViewController pickFromLibrary:self.mainViewController];
    
    // press cancel button
    [self.mainViewController.libraryViewController cancel:[self.mainViewController.libraryViewController.view viewWithTag:0]];
    
    // check that no image was actually selected
    STAssertNil(self.mainViewController.imageView.image, @"There is an image selected");
}

- (void)testPickingFromLibrary
{
    // set up library view controller
    [self.mainViewController pickFromLibrary:self.mainViewController];
    
    // make sure there are enough photos to choose from
    if ([self.mainViewController.libraryViewController.library.imagePaths count] > 1) {
        // remember the second image
        NSString *imageName = [self.mainViewController.libraryViewController.library.imagePaths objectAtIndex:1];
        UIImage *image = [UIImage imageNamed:imageName];
        
        // pick the second image
        [self.mainViewController.libraryViewController selectImage:[self.mainViewController.libraryViewController.view viewWithTag:1]];
                
        // check that the correct image is selected in the main view
        STAssertTrue(self.mainViewController.imageView.image == image, @"Image in main view controller doesn't match selection");
    }    
}

- (void)testTyping
{
    // test that entering in capitalized text works
    self.mainViewController.topText.text = @"TEST";
    [self.mainViewController textFieldShouldReturn:self.mainViewController.topText];
    STAssertTrue([self.mainViewController.topText.text isEqualToString:@"TEST"], @"Text not entered.");
    
    
    // test that app will capitalize uncapitalized input
    self.mainViewController.bottomText.text = @"test2";
    [self.mainViewController textFieldShouldReturn:self.mainViewController.bottomText];
    STAssertTrue([self.mainViewController.bottomText.text isEqualToString:@"TEST2"], @"Text not capitalized.");

}

- (void)testMeme
{    
    // pick some options in the main view controller
    self.mainViewController.topText.text = @"TOP TEXT";
    self.mainViewController.bottomText.text = @"BOTTOM TEXT";
    self.mainViewController.textColorSelector.selectedSegmentIndex = RED;
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: @"http://1.bp.blogspot.com/-iJH4pMYgtO4/TZu2-E4HzoI/AAAAAAAAAK4/tGI2hm6_1A8/s1600/14.%2BRickroll.jpg"]];
    UIImage * image = [UIImage imageWithData: imageData];
    self.mainViewController.imageView.image = image;
    
    // check that the top text is there
    STAssertTrue([self.mainViewController.topText.text isEqualToString:@"TOP TEXT"], [NSString stringWithFormat:@"Top text incorrect. The text here is %@", self.mainViewController.topText.text]);
    
    // set up a flipside view controller
    [self.mainViewController showMeme:self.mainViewController];
    self.mainViewController.flipsideViewController = (FlipsideViewController *)[self.mainViewController presentedViewController];
    
    // check that text transferred
    STAssertTrue([[self.mainViewController.flipsideViewController.topText text] isEqualToString:@"TOP TEXT"], [NSString stringWithFormat:@"Top text should be 'TOP TEXT'. The text here is %@", [self.mainViewController.flipsideViewController.topText text]]);
     STAssertTrue([[self.mainViewController.flipsideViewController.bottomText text] isEqualToString:@"BOTTOM TEXT"], [NSString stringWithFormat:@"Top text should be 'BOTTOM TEXT'. The text here is %@", self.mainViewController.flipsideViewController.bottomText.text]);
    
    // check that text color transferred
    STAssertTrue([[self.mainViewController.flipsideViewController.topText textColor] isEqual:[UIColor redColor]], @"Text color should be red.");
    STAssertTrue([[self.mainViewController.flipsideViewController.bottomText textColor] isEqual:[UIColor redColor]], @"Text color should be red.");
    
    // check that image transferred
    STAssertNotNil(self.mainViewController.flipsideViewController.imageView.image, @"There is no image here.");
    
    [self.mainViewController.flipsideViewController done:[self.mainViewController.flipsideViewController.view viewWithTag:0]];
    
}


@end
